class Walker
{
  float x = 0;
  float y = 50;
  float r_t = 100;
  float g_t = 150;
  float b_t = 200;
  float d = 250;
  
  void render()
  {
    float choiceX = map(noise(x), 0, 1, -width / 2, width / 2);
    float choiceY = map(noise(y), 0, 1, -height / 2, height / 2);
    
    circle(choiceX, choiceY, map(noise(d), 0, 1, 10, 30));
    
    fill (map(noise(r_t), 0, 1, 0, 255), map(noise(g_t), 0, 1, 0, 255), map(noise(b_t), 0, 1, 0, 255), 255); 
    
    x += 0.01f;
    y += 0.01f;
    d += 0.1f;
    r_t += 0.1f;
    g_t += 0.1f;
    b_t += 0.1f;
  }
}
