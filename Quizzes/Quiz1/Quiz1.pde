void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 100),
          0, 0, 0,
          0, -1, 0);
}

void draw()
{
  
  float mean = 0;
  float std = 100;
  float gauss1 = randomGaussian();
  float gauss2 = randomGaussian();
  
  float x = std * gauss1 + mean;
  float y = floor(random(height / 2));
  float z = std * gauss2 + mean;
  
  //noStroke();
  fill(random(255), random(255), random(255), random(10, 50));
  circle(x, y, z);
  
  if (frameCount > 1000)
  {
    background(255);
  }
}
