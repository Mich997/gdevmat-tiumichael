class Walker
{
  float x;
  float y;
  
  void render()
  {
    circle(x, y, 30);
  }
  
  void balanceBeam()
  {
    float choiceX = floor(random(2));
    float choiceY = floor(random(2));
    
    if (choiceY == 0)
    {
      y++;
    }
    else if (choiceY == 1)
    {
      y--;
    }
    
    if (choiceX == 0)
    {
      x++;
    }
    else if (choiceX == 1)
    {
      x--;
    }
    
    fill(random(255), random(255), random(255), random(255));
  }
}
