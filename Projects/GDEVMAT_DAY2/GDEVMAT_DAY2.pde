void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 100),
          0, 0, 0,
          0, -1, 0);
}

Walker walk = new Walker();

void draw()
{
  //walk.render();
  
  //float rand = floor(random(4));
  //println(rand);
  
  walk.balanceBeam();
  walk.render();
}
