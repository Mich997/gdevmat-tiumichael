void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
}

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  
  return new Vector2(x, y);
}

void draw()
{
  background(0);
  
  Vector2 mouse = mousePos();
  mouse.normalize();
  mouse.multi(350);
  
  strokeWeight(15);
  stroke(0, 255, 0); // green
  line(0, 0, mouse.x, mouse.y);
  
  stroke(255, 255, 255); // white
  line(mouse.x / 10, mouse.y / 10, -mouse.x / 10, -mouse.y / 10);
  
  stroke(0, 0, 255); // blue
  line(0, 0, -mouse.x, -mouse.y);
}
