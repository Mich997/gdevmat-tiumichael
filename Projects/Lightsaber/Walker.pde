class Walker
{
  public Vector2 position;
  public float scalar = 30;
  
  Walker()
  {
    position = new Vector2();
  }
  
  Walker(Vector2 position)
  {
    this.position = position;
  }
  
  void render()
  {
    fill(random(255), random(255), random(255), random(255));
    circle(position.x, position.y, 40);
  }
}
